**Simple scene rendered with Vulkan**
The rendering framework taken from book 'Vulkan Cookbook'.
https://www.packtpub.com/game-development/vulkan-cookbook


**Windows build**


mkdir build && cd build && cmake .. -DVK_USE_PLATFORM=WIN32 -G "Visual Studio 15 2017 Win64"

![Scheme](raytracing_demo.png)