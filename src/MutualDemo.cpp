#include "MutualDemo.h"

static const ViewportInfo viewport_infos = {
    {
        {0.0f, 0.0f, 512.0f, 512.0f, 0.0f, 1.0f},
    },
    {{{0, 0}, {512, 512}}}};

bool MutualDemo::Initialize(WindowParameters window_parameters) {
  Camera = OrbitingCamera(Vector3{0.0f, 1.f, 0.0f}, 5.0f, -60.0f, -30.0f);
  LightSource = OrbitingCamera(Vector3{0.0f, 0.0f, 0.0f}, 8.0f, 60.0f, -30.0f);

  if (!InitializeVulkan(window_parameters)) {
    return false;
  }

  return initRaytracePipeline() && initRasterizePipeline() &&
         initFramerateLayerPipeline();
}

bool MutualDemo::Draw() {
  bool res = false;
  if (m_rndRaytracer)
    res = renderRasterizer();
  else
    res = renderRaytracer();

  return res & renderFrameRateLayer();
}

bool MutualDemo::Resize() {
  // TODO handle resolution change
  if (!CreateSwapchain()) {
    return false;
  }

  if (IsReady()) {
    if (!UpdateStagingBuffer(true)) {
      return false;
    }
  }
  return true;
}

bool MutualDemo::UpdateStagingBuffer(bool force) {
  if (force) {
    rastUpdateUniformBuffer = true;
    rtUpdateUniformBuffer = true;

    Matrix4x4 light_view_matrix = LightSource.GetMatrix();
    Matrix4x4 scene_view_matrix = Camera.GetMatrix();
    Matrix4x4 perspective_matrix = PreparePerspectiveProjectionMatrix(
        static_cast<float>(Swapchain.Size.width) /
            static_cast<float>(Swapchain.Size.height),
        50.0f, 0.5f, 10.0f);

    std::vector<float> data;
    data.insert(data.end(), &light_view_matrix[0], &light_view_matrix[0] + 16);
    data.insert(data.end(), &scene_view_matrix[0], &scene_view_matrix[0] + 16);
    data.insert(data.end(), &perspective_matrix[0], &perspective_matrix[0] + 16);

    if (!MapUpdateAndUnmapHostVisibleMemory(
            *LogicalDevice, *rastStagingBufferMemory, 0,
            sizeof(data[0]) * data.size(), &data[0], true, nullptr)) {
      return false;
    }

    Vector3 cam = Camera.GetPosition();
    Vector3 light = LightSource.GetPosition();

    data.clear();
    data.insert(data.end(), &cam[0], &cam[0] + 3);
    data.insert(data.end(), &light[0], &light[0] + 3);

    if (!MapUpdateAndUnmapHostVisibleMemory(
            *LogicalDevice, *rtStagingBufferMemory, 0,
            sizeof(data[0]) * data.size(), &data[0], true, nullptr)) {
      return false;
    }
  }
  return true;
}

bool MutualDemo::renderRasterizer() {
  auto prepare_frame = [&](VkCommandBuffer command_buffer,
                           uint32_t swapchain_image_index,
                           VkFramebuffer framebuffer) {
    if (!BeginCommandBufferRecordingOperation(command_buffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,nullptr)) {
      return false;
    }

    if (rastUpdateUniformBuffer) {
      rastUpdateUniformBuffer = false;

      BufferTransition pre_transfer_transition = {
          *rastUniformBuffer,           // VkBuffer         Buffer
          VK_ACCESS_UNIFORM_READ_BIT,   // VkAccessFlags    CurrentAccess
          VK_ACCESS_TRANSFER_WRITE_BIT, // VkAccessFlags    NewAccess
          VK_QUEUE_FAMILY_IGNORED,      // uint32_t         CurrentQueueFamily
          VK_QUEUE_FAMILY_IGNORED       // uint32_t         NewQueueFamily
      };
      SetBufferMemoryBarrier(
          command_buffer, VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
          VK_PIPELINE_STAGE_TRANSFER_BIT, {pre_transfer_transition});

      std::vector<VkBufferCopy> regions = {{
          0,                     // VkDeviceSize     srcOffset
          0,                     // VkDeviceSize     dstOffset
          3 * 16 * sizeof(float) // VkDeviceSize     size
      }};
      CopyDataBetweenBuffers(command_buffer, *rastStagingBuffer,
                             *rastUniformBuffer, regions);

      BufferTransition post_transfer_transition = {
          *rastUniformBuffer,           // VkBuffer         Buffer
          VK_ACCESS_TRANSFER_WRITE_BIT, // VkAccessFlags    CurrentAccess
          VK_ACCESS_UNIFORM_READ_BIT,   // VkAccessFlags    NewAccess
          VK_QUEUE_FAMILY_IGNORED,      // uint32_t         CurrentQueueFamily
          VK_QUEUE_FAMILY_IGNORED       // uint32_t         NewQueueFamily
      };
      SetBufferMemoryBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
                             {post_transfer_transition});
    }

    // Shadow map generation

    BeginRenderPass(command_buffer, *rastShadowMapRenderPass,
                    *rastShadowMap.Framebuffer,
                    {{
                         0,
                         0,
                     },
                     {512, 512}},
                    {{1.0f, 0}}, VK_SUBPASS_CONTENTS_INLINE);

    BindVertexBuffers(command_buffer, 0, {{*rastVertexBuffer, 0}});

    BindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                       *rastPipelineLayout, 0, rastDescriptorSets, {});

    BindPipelineObject(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                       *rastShadowMapPipeline);

    DrawGeometry(command_buffer,
                 rastScene[0].Parts[0].VertexCount +
                     rastScene[1].Parts[0].VertexCount,
                 1, 0, 0);

    EndRenderPass(command_buffer);

    if (PresentQueue.FamilyIndex != GraphicsQueue.FamilyIndex) {
      ImageTransition image_transition_before_drawing = {
          Swapchain.Images[swapchain_image_index], // VkImage Image
          VK_ACCESS_MEMORY_READ_BIT, // VkAccessFlags        CurrentAccess
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags NewAccess
          VK_IMAGE_LAYOUT_UNDEFINED, // VkImageLayout        CurrentLayout
          VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // VkImageLayout NewLayout
          PresentQueue.FamilyIndex,  // uint32_t             CurrentQueueFamily
          GraphicsQueue.FamilyIndex, // uint32_t             NewQueueFamily
          VK_IMAGE_ASPECT_COLOR_BIT  // VkImageAspectFlags   Aspect
      };
      SetImageMemoryBarrier(command_buffer,
                            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                            {image_transition_before_drawing});
    }

    // Drawing
    BeginRenderPass(command_buffer, *rastSceneRenderPass, framebuffer,
                    {{0, 0}, Swapchain.Size},
                    {{0.1f, 0.2f, 0.3f, 1.0f}, {1.0f, 0}},
                    VK_SUBPASS_CONTENTS_INLINE);

    BindPipelineObject(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                       *rastScenePipeline);

    VkViewport viewport = {
        0.0f,                                      // float    x
        0.0f,                                      // float    y
        static_cast<float>(Swapchain.Size.width),  // float    width
        static_cast<float>(Swapchain.Size.height), // float    height
        0.0f,                                      // float    minDepth
        1.0f,                                      // float    maxDepth
    };
    SetViewportStateDynamically(command_buffer, 0, {viewport});

    VkRect2D scissor = {{
                            // VkOffset2D     offset
                            0, // int32_t        x
                            0  // int32_t        y
                        },
                        {
                            // VkExtent2D     extent
                            Swapchain.Size.width, // uint32_t       width
                            Swapchain.Size.height // uint32_t       height
                        }};
    SetScissorStateDynamically(command_buffer, 0, {scissor});

    ProvideDataToShadersThroughPushConstants(
        command_buffer, *rastPipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0,
        sizeof(float) * 4, &LightSource.GetPosition()[0]);

    DrawGeometry(command_buffer,
                 rastScene[0].Parts[0].VertexCount +
                     rastScene[1].Parts[0].VertexCount,
                 1, 0, 0);

    EndRenderPass(command_buffer);

    if (PresentQueue.FamilyIndex != GraphicsQueue.FamilyIndex) {
      ImageTransition image_transition_before_present = {
          Swapchain.Images[swapchain_image_index], // VkImage Image
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags CurrentAccess
          VK_ACCESS_MEMORY_READ_BIT,       // VkAccessFlags        NewAccess
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // VkImageLayout CurrentLayout
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // VkImageLayout        NewLayout
          GraphicsQueue.FamilyIndex,       // uint32_t CurrentQueueFamily
          PresentQueue.FamilyIndex, // uint32_t             NewQueueFamily
          VK_IMAGE_ASPECT_COLOR_BIT // VkImageAspectFlags   Aspect
      };
      SetImageMemoryBarrier(command_buffer,
                            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                            VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                            {image_transition_before_present});
    }

    if (!EndCommandBufferRecordingOperation(command_buffer)) {
      return false;
    }
    return true;
  };

  WaitForAllSubmittedCommandsToBeFinished(*LogicalDevice);

  return IncreasePerformanceThroughIncreasingTheNumberOfSeparatelyRenderedFrames(
      *LogicalDevice, GraphicsQueue.Handle, PresentQueue.Handle,
      *Swapchain.Handle, Swapchain.Size, Swapchain.ImageViewsRaw,
      *rastSceneRenderPass, {}, prepare_frame, FramesResources);
}

bool MutualDemo::renderRaytracer() {
  auto prepare_frame = [&](VkCommandBuffer command_buffer,
                           uint32_t swapchain_image_index,
                           VkFramebuffer framebuffer) {
    if (!BeginCommandBufferRecordingOperation(
            command_buffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
            nullptr)) {
      return false;
    }

    if (rtUpdateUniformBuffer) {
      rtUpdateUniformBuffer = false;

      BufferTransition pre_transfer_transition = {
          *rtUniformBuffer,             // VkBuffer         Buffer
          VK_ACCESS_UNIFORM_READ_BIT,   // VkAccessFlags    CurrentAccess
          VK_ACCESS_TRANSFER_WRITE_BIT, // VkAccessFlags    NewAccess
          VK_QUEUE_FAMILY_IGNORED,      // uint32_t         CurrentQueueFamily
          VK_QUEUE_FAMILY_IGNORED       // uint32_t         NewQueueFamily
      };
      SetBufferMemoryBarrier(
          command_buffer, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
          VK_PIPELINE_STAGE_TRANSFER_BIT, {pre_transfer_transition});

      std::vector<VkBufferCopy> regions = {{0, 0, 2 * 3 * sizeof(float)}};
      CopyDataBetweenBuffers(command_buffer, *rtStagingBuffer, *rtUniformBuffer,
                             regions);
      BufferTransition post_transfer_transition = {
          *rtUniformBuffer, VK_ACCESS_TRANSFER_WRITE_BIT,
          VK_ACCESS_UNIFORM_READ_BIT, VK_QUEUE_FAMILY_IGNORED,
          VK_QUEUE_FAMILY_IGNORED};
      SetBufferMemoryBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                             {post_transfer_transition});
    }

    if (PresentQueue.FamilyIndex != GraphicsQueue.FamilyIndex) {
      ImageTransition image_transition_before_drawing = {
          Swapchain.Images[swapchain_image_index], // VkImage Image
          VK_ACCESS_MEMORY_READ_BIT, // VkAccessFlags        CurrentAccess
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags NewAccess
          VK_IMAGE_LAYOUT_UNDEFINED, // VkImageLayout        CurrentLayout
          VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // VkImageLayout NewLayout
          PresentQueue.FamilyIndex,  // uint32_t             CurrentQueueFamily
          GraphicsQueue.FamilyIndex, // uint32_t             NewQueueFamily
          VK_IMAGE_ASPECT_COLOR_BIT  // VkImageAspectFlags   Aspect
      };
      SetImageMemoryBarrier(command_buffer,
                            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                            {image_transition_before_drawing});
    }

    // Drawing
    BeginRenderPass(
        command_buffer, *rtRenderPass, framebuffer, {{0, 0}, Swapchain.Size},
        {{0.1f, 0.2f, 0.3f, 1.0f}, {1.0f, 0}}, VK_SUBPASS_CONTENTS_INLINE);

    VkViewport viewport = {
        0.0f,                                      // float    x
        0.0f,                                      // float    y
        static_cast<float>(Swapchain.Size.width),  // float    width
        static_cast<float>(Swapchain.Size.height), // float    height
        0.0f,                                      // float    minDepth
        1.0f,                                      // float    maxDepth
    };
    SetViewportStateDynamically(command_buffer, 0, {viewport});

    VkRect2D scissor = {{
                            // VkOffset2D     offset
                            0, // int32_t        x
                            0  // int32_t        y
                        },
                        {
                            // VkExtent2D     extent
                            Swapchain.Size.width, // uint32_t       width
                            Swapchain.Size.height // uint32_t       height
                        }};
    SetScissorStateDynamically(command_buffer, 0, {scissor});

    BindVertexBuffers(command_buffer, 0, {{*rtVertexBuffer, 0}});

    BindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                       *rtPipelineLayout, 0, rtDescriptorSets, {});

    BindPipelineObject(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                       *rtPipeline);

    // for( size_t i = 0; i < Model.Parts.size(); ++i ) {
    DrawGeometry(command_buffer, 6, 1, 0, 0);
    //}

    EndRenderPass(command_buffer);

    if (PresentQueue.FamilyIndex != GraphicsQueue.FamilyIndex) {
      ImageTransition image_transition_before_present = {
          Swapchain.Images[swapchain_image_index], // VkImage Image
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags CurrentAccess
          VK_ACCESS_MEMORY_READ_BIT,       // VkAccessFlags        NewAccess
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // VkImageLayout CurrentLayout
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // VkImageLayout        NewLayout
          GraphicsQueue.FamilyIndex,       // uint32_t CurrentQueueFamily
          PresentQueue.FamilyIndex, // uint32_t             NewQueueFamily
          VK_IMAGE_ASPECT_COLOR_BIT // VkImageAspectFlags   Aspect
      };
      SetImageMemoryBarrier(command_buffer,
                            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                            VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                            {image_transition_before_present});
    }

    if (!EndCommandBufferRecordingOperation(command_buffer)) {
      return false;
    }
    return true;
  };

  return IncreasePerformanceThroughIncreasingTheNumberOfSeparatelyRenderedFrames(
      *LogicalDevice, GraphicsQueue.Handle, PresentQueue.Handle,
      *Swapchain.Handle, Swapchain.Size, Swapchain.ImageViewsRaw, *rtRenderPass,
      {}, prepare_frame, FramesResources);
}

void MutualDemo::OnMouseEvent(MouseStateParameters &me) {
  bool force = false;
  if (me.Buttons[0].IsPressed) {
    Camera.RotateHorizontally(0.5f * me.Position.Delta.X);
    Camera.RotateVertically(-0.5f * me.Position.Delta.Y);
    force = true;
  }

  if (me.Buttons[1].IsPressed && false) { //Disabled
    LightSource.RotateHorizontally(0.5f * me.Position.Delta.X);
    LightSource.RotateVertically(-0.5f * me.Position.Delta.Y);
    force = true;
  }

  UpdateStagingBuffer(force);
}

void MutualDemo::SwitchMode() { m_rndRaytracer = !m_rndRaytracer; }

bool MutualDemo::initRaytracePipeline() {
  // Vertex data
  std::vector<float> vertices = {-1.0f, -1.0f, 0., 0., -1.0f, 1.0f, 0., 1.,
                                 1.0f,  -1.0f, 1., 0., 1.0f,  -1.f, 1., 0.,
                                 -1.f,  1.f,   0., 1., 1.f,   1.f,  1., 1.};

  InitVkDestroyer(LogicalDevice, rtVertexBuffer);
  if (!CreateBuffer(*LogicalDevice, sizeof(vertices[0]) * vertices.size(),
                    VK_BUFFER_USAGE_TRANSFER_DST_BIT |
                        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                    *rtVertexBuffer)) {
    return false;
  }

  InitVkDestroyer(LogicalDevice, rtVertexBufferMemory);
  if (!AllocateAndBindMemoryObjectToBuffer(
          PhysicalDevice, *LogicalDevice, *rtVertexBuffer,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, *rtVertexBufferMemory)) {
    return false;
  }

  if (!UseStagingBufferToUpdateBufferWithDeviceLocalMemoryBound(
          PhysicalDevice, *LogicalDevice, sizeof(vertices[0]) * vertices.size(),
          &vertices[0], *rtVertexBuffer, 0, 0,
          VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
          GraphicsQueue.Handle, FramesResources.front().CommandBuffer, {})) {
    return false;
  }

  // Staging buffer
  InitVkDestroyer(LogicalDevice, rtStagingBuffer);
  if (!CreateBuffer(*LogicalDevice, 2 * 3 * sizeof(float),
                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT, *rtStagingBuffer)) {
    return false;
  }
  InitVkDestroyer(LogicalDevice, rtStagingBufferMemory);
  if (!AllocateAndBindMemoryObjectToBuffer(
          PhysicalDevice, *LogicalDevice, *rtStagingBuffer,
          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, *rtStagingBufferMemory)) {
    return false;
  }

  // Uniform buffer
  InitVkDestroyer(LogicalDevice, rtUniformBuffer);
  InitVkDestroyer(LogicalDevice, rtUniformBufferMemory);
  if (!CreateUniformBuffer(
          PhysicalDevice, *LogicalDevice, 2 * 3 * sizeof(float),
          VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
          *rtUniformBuffer, *rtUniformBufferMemory)) {
    return false;
  }

  // if( !UpdateStagingBuffer( true ) ) {
  //  return false;
  //}

  // Descriptor set with uniform buffer
  VkDescriptorSetLayoutBinding descriptor_set_layout_binding = {
      0,                                 // uint32_t             binding
      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType descriptorType
      1,                                 // uint32_t             descriptorCount
      VK_SHADER_STAGE_FRAGMENT_BIT,      // VkShaderStageFlags   stageFlags
      nullptr // const VkSampler    * pImmutableSamplers
  };
  InitVkDestroyer(LogicalDevice, rtDescriptorSetLayout);
  if (!CreateDescriptorSetLayout(*LogicalDevice,
                                 {descriptor_set_layout_binding},
                                 *rtDescriptorSetLayout)) {
    return false;
  }

  VkDescriptorPoolSize descriptor_pool_size = {
      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType     type
      1                                  // uint32_t             descriptorCount
  };
  InitVkDestroyer(LogicalDevice, rtDescriptorPool);
  if (!CreateDescriptorPool(*LogicalDevice, false, 1, {descriptor_pool_size},
                            *rtDescriptorPool)) {
    return false;
  }

  if (!AllocateDescriptorSets(*LogicalDevice, *rtDescriptorPool,
                              {*rtDescriptorSetLayout}, rtDescriptorSets)) {
    return false;
  }

  BufferDescriptorInfo buffer_descriptor_update = {
      rtDescriptorSets[0], // VkDescriptorSet TargetDescriptorSet
      0, // uint32_t                             TargetDescriptorBinding
      0, // uint32_t                             TargetArrayElement
      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType
      // TargetDescriptorType
      {// std::vector<VkDescriptorBufferInfo>  BufferInfos
       {
           *rtUniformBuffer, // VkBuffer                             buffer
           0,                // VkDeviceSize                         offset
           VK_WHOLE_SIZE     // VkDeviceSize                         range
       }}};

  UpdateDescriptorSets(*LogicalDevice, {}, {buffer_descriptor_update}, {}, {});

  // Render pass
  std::vector<VkAttachmentDescription> attachment_descriptions = {{
      0,                            // VkAttachmentDescriptionFlags     flags
      Swapchain.Format,             // VkFormat                         format
      VK_SAMPLE_COUNT_1_BIT,        // VkSampleCountFlagBits            samples
      VK_ATTACHMENT_LOAD_OP_CLEAR,  // VkAttachmentLoadOp               loadOp
      VK_ATTACHMENT_STORE_OP_STORE, // VkAttachmentStoreOp storeOp
      VK_ATTACHMENT_LOAD_OP_DONT_CARE,  // VkAttachmentLoadOp stencilLoadOp
      VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp stencilStoreOp
      VK_IMAGE_LAYOUT_UNDEFINED,        // VkImageLayout initialLayout
      VK_IMAGE_LAYOUT_PRESENT_SRC_KHR   // VkImageLayout finalLayout
  }};

  std::vector<SubpassParameters> subpass_parameters = {{
      VK_PIPELINE_BIND_POINT_GRAPHICS, // VkPipelineBindPoint PipelineType
      {}, // std::vector<VkAttachmentReference>   InputAttachments
      {   // std::vector<VkAttachmentReference>   ColorAttachments
       {
           0, // uint32_t                             attachment
           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // VkImageLayout layout
       }},
      {}, // std::vector<VkAttachmentReference>   ResolveAttachments
      {}, // VkAttachmentReference const        * DepthStencilAttachment
      {}  // std::vector<uint32_t>                PreserveAttachments
  }};

  std::vector<VkSubpassDependency> subpass_dependencies = {
      {
          VK_SUBPASS_EXTERNAL, // uint32_t                   srcSubpass
          0,                   // uint32_t                   dstSubpass
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // VkPipelineStageFlags
          // srcStageMask
          VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // VkPipelineStageFlags
          // dstStageMask
          VK_ACCESS_MEMORY_READ_BIT,            // VkAccessFlags srcAccessMask
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags dstAccessMask
          VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags dependencyFlags
      },
      {
          0,                   // uint32_t                   srcSubpass
          VK_SUBPASS_EXTERNAL, // uint32_t                   dstSubpass
          VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // VkPipelineStageFlags
          // srcStageMask
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // VkPipelineStageFlags
          // dstStageMask
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags srcAccessMask
          VK_ACCESS_MEMORY_READ_BIT,            // VkAccessFlags dstAccessMask
          VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags dependencyFlags
      }};

  InitVkDestroyer(LogicalDevice, rtRenderPass);
  if (!CreateRenderPass(*LogicalDevice, attachment_descriptions,
                        subpass_parameters, subpass_dependencies,
                        *rtRenderPass)) {
    return false;
  }

  // Graphics pipeline

  InitVkDestroyer(LogicalDevice, rtPipelineLayout);
  if (!CreatePipelineLayout(*LogicalDevice, {*rtDescriptorSetLayout}, {},
                            *rtPipelineLayout)) {
    return false;
  }

  std::vector<unsigned char> vertex_shader_spirv;
  if (!GetBinaryFileContents("Data/Shaders/raytracing.vert",
                             vertex_shader_spirv)) {
    return false;
  }

  VkDestroyer(VkShaderModule) vertex_shader_module;
  InitVkDestroyer(LogicalDevice, vertex_shader_module);
  if (!CreateShaderModule(*LogicalDevice, vertex_shader_spirv,
                          *vertex_shader_module)) {
    return false;
  }

  std::vector<unsigned char> fragment_shader_spirv;
  if (!GetBinaryFileContents("Data/Shaders/raytracing.frag",
                             fragment_shader_spirv)) {
    return false;
  }
  VkDestroyer(VkShaderModule) fragment_shader_module;
  InitVkDestroyer(LogicalDevice, fragment_shader_module);
  if (!CreateShaderModule(*LogicalDevice, fragment_shader_spirv,
                          *fragment_shader_module)) {
    return false;
  }

  std::vector<ShaderStageParameters> shader_stage_params = {
      {
          VK_SHADER_STAGE_VERTEX_BIT, // VkShaderStageFlagBits ShaderStage
          *vertex_shader_module, // VkShaderModule               ShaderModule
          "main",                // char const                 * EntryPointName
          nullptr // VkSpecializationInfo const * SpecializationInfo
      },
      {
          VK_SHADER_STAGE_FRAGMENT_BIT, // VkShaderStageFlagBits ShaderStage
          *fragment_shader_module,      // VkShaderModule ShaderModule
          "main", // char const                 * EntryPointName
          nullptr // VkSpecializationInfo const * SpecializationInfo
      }};

  std::vector<VkPipelineShaderStageCreateInfo> shader_stage_create_infos;
  SpecifyPipelineShaderStages(shader_stage_params, shader_stage_create_infos);

  std::vector<VkVertexInputBindingDescription>
      vertex_input_binding_descriptions = {{
          0,                          // uint32_t                     binding
          4 * sizeof(float),          // uint32_t                     stride
          VK_VERTEX_INPUT_RATE_VERTEX // VkVertexInputRate inputRate
      }};

  std::vector<VkVertexInputAttributeDescription> vertex_attribute_descriptions =
      {{
           0,                          // uint32_t   location
           0,                          // uint32_t   binding
           VK_FORMAT_R32G32B32_SFLOAT, // VkFormat   format
           0                           // uint32_t   offset
       },
       {
           1,                          // uint32_t   location
           0,                          // uint32_t   binding
           VK_FORMAT_R32G32B32_SFLOAT, // VkFormat   format
           2 * sizeof(float)           // uint32_t   offset
       }};

  VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info;
  SpecifyPipelineVertexInputState(vertex_input_binding_descriptions,
                                  vertex_attribute_descriptions,
                                  vertex_input_state_create_info);

  VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info;
  SpecifyPipelineInputAssemblyState(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, false,
                                    input_assembly_state_create_info);

  VkPipelineViewportStateCreateInfo viewport_state_create_info;
  SpecifyPipelineViewportAndScissorTestState(viewport_infos,
                                             viewport_state_create_info);

  VkPipelineRasterizationStateCreateInfo rasterization_state_create_info;
  SpecifyPipelineRasterizationState(
      false, false, VK_POLYGON_MODE_FILL, VK_CULL_MODE_BACK_BIT,
      VK_FRONT_FACE_COUNTER_CLOCKWISE, false, 0.0f, 0.0f, 0.0f, 1.0f,
      rasterization_state_create_info);

  VkPipelineMultisampleStateCreateInfo multisample_state_create_info;
  SpecifyPipelineMultisampleState(VK_SAMPLE_COUNT_1_BIT, false, 0.0f, nullptr,
                                  false, false, multisample_state_create_info);

  VkPipelineDepthStencilStateCreateInfo depth_stencil_state_create_info;
  SpecifyPipelineDepthAndStencilState(true, true, VK_COMPARE_OP_LESS_OR_EQUAL,
                                      false, 0.0f, 1.0f, false, {}, {},
                                      depth_stencil_state_create_info);

  std::vector<VkPipelineColorBlendAttachmentState> attachment_blend_states = {
      {false,               // VkBool32                 blendEnable
       VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcColorBlendFactor
       VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstColorBlendFactor
       VK_BLEND_OP_ADD,     // VkBlendOp                colorBlendOp
       VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcAlphaBlendFactor
       VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstAlphaBlendFactor
       VK_BLEND_OP_ADD,     // VkBlendOp                alphaBlendOp
       VK_COLOR_COMPONENT_R_BIT | // VkColorComponentFlags    colorWriteMask
           VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
           VK_COLOR_COMPONENT_A_BIT}};
  VkPipelineColorBlendStateCreateInfo blend_state_create_info;
  SpecifyPipelineBlendState(false, VK_LOGIC_OP_COPY, attachment_blend_states,
                            {1.0f, 1.0f, 1.0f, 1.0f}, blend_state_create_info);

  std::vector<VkDynamicState> dynamic_states = {VK_DYNAMIC_STATE_VIEWPORT,
                                                VK_DYNAMIC_STATE_SCISSOR};
  VkPipelineDynamicStateCreateInfo dynamic_state_create_info;
  SpecifyPipelineDynamicStates(dynamic_states, dynamic_state_create_info);

  VkGraphicsPipelineCreateInfo pipeline_create_info;
  SpecifyGraphicsPipelineCreationParameters(
      0, shader_stage_create_infos, vertex_input_state_create_info,
      input_assembly_state_create_info, nullptr, &viewport_state_create_info,
      rasterization_state_create_info, &multisample_state_create_info,
      &depth_stencil_state_create_info, &blend_state_create_info,
      &dynamic_state_create_info, *rtPipelineLayout, *rtRenderPass, 0,
      VK_NULL_HANDLE, -1, pipeline_create_info);

  std::vector<VkPipeline> pipeline;
  if (!CreateGraphicsPipelines(*LogicalDevice, {pipeline_create_info},
                               VK_NULL_HANDLE, pipeline)) {
    return false;
  }
  InitVkDestroyer(LogicalDevice, rtPipeline);
  *rtPipeline = pipeline[0];

  return true;
}

bool MutualDemo::initRasterizePipeline() {
  // Vertex data
  if (!Load3DModelFromObjFile("Data/Models/sphere.obj", true, false, false,
                              true, rastScene[0])) {
    return false;
  }

  if (!Load3DModelFromObjFile("Data/Models/plane.obj", true, false, false,
                              false, rastScene[1])) {
    return false;
  }

  std::vector<float> vertex_data(rastScene[0].Data);
  vertex_data.insert(vertex_data.end(), rastScene[1].Data.begin(),
                     rastScene[1].Data.end());

  InitVkDestroyer(LogicalDevice, rastVertexBuffer);
  if (!CreateBuffer(*LogicalDevice, sizeof(vertex_data[0]) * vertex_data.size(),
                    VK_BUFFER_USAGE_TRANSFER_DST_BIT |
                        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                    *rastVertexBuffer)) {
    return false;
  }

  InitVkDestroyer(LogicalDevice, rastVertexBufferMemory);
  if (!AllocateAndBindMemoryObjectToBuffer(
          PhysicalDevice, *LogicalDevice, *rastVertexBuffer,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, *rastVertexBufferMemory)) {
    return false;
  }

  if (!UseStagingBufferToUpdateBufferWithDeviceLocalMemoryBound(
          PhysicalDevice, *LogicalDevice,
          sizeof(vertex_data[0]) * vertex_data.size(), &vertex_data[0],
          *rastVertexBuffer, 0, 0, VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
          GraphicsQueue.Handle, FramesResources.front().CommandBuffer, {})) {
    return false;
  }

  // Staging buffer
  InitVkDestroyer(LogicalDevice, rastStagingBuffer);
  if (!CreateBuffer(*LogicalDevice, 3 * 16 * sizeof(float),
                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT, *rastStagingBuffer)) {
    return false;
  }
  InitVkDestroyer(LogicalDevice, rastStagingBufferMemory);
  if (!AllocateAndBindMemoryObjectToBuffer(
          PhysicalDevice, *LogicalDevice, *rastStagingBuffer,
          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, *rastStagingBufferMemory)) {
    return false;
  }

  // Uniform buffer
  InitVkDestroyer(LogicalDevice, rastUniformBuffer);
  InitVkDestroyer(LogicalDevice, rastUniformBufferMemory);
  if (!CreateUniformBuffer(
          PhysicalDevice, *LogicalDevice, 3 * 16 * sizeof(float),
          VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
          *rastUniformBuffer, *rastUniformBufferMemory)) {
    return false;
  }

  if (!UpdateStagingBuffer(true)) {
    return false;
  }

  // Image in which shadow map will be stored

  InitVkDestroyer(LogicalDevice, rastShadowMap.Image);
  InitVkDestroyer(LogicalDevice, rastShadowMap.Memory);
  InitVkDestroyer(LogicalDevice, rastShadowMap.View);
  InitVkDestroyer(LogicalDevice, rastShadowMapSampler);
  if (!CreateCombinedImageSampler(
          PhysicalDevice, *LogicalDevice, VK_IMAGE_TYPE_2D, DepthFormat,
          {512, 512, 1}, 1, 1,
          VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT |
              VK_IMAGE_USAGE_SAMPLED_BIT,
          false, VK_IMAGE_VIEW_TYPE_2D, VK_IMAGE_ASPECT_DEPTH_BIT,
          VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_NEAREST,
          VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
          VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
          VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, 0.0f, false, 1.0f, false,
          VK_COMPARE_OP_ALWAYS, 0.0f, 1.0f, VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK,
          false, *rastShadowMapSampler, *rastShadowMap.Image,
          *rastShadowMap.Memory, *rastShadowMap.View)) {
    return false;
  }

  // Descriptor set

  std::vector<VkDescriptorSetLayoutBinding> descriptor_set_layout_bindings = {
      {
          0,                                 // uint32_t             binding
          VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType
          // descriptorType
          1,                          // uint32_t             descriptorCount
          VK_SHADER_STAGE_VERTEX_BIT, // VkShaderStageFlags   stageFlags
          nullptr                     // const VkSampler    * pImmutableSamplers
      },
      {
          1, // uint32_t             binding
          VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, // VkDescriptorType
          // descriptorType
          1,                            // uint32_t             descriptorCount
          VK_SHADER_STAGE_FRAGMENT_BIT, // VkShaderStageFlags   stageFlags
          nullptr // const VkSampler    * pImmutableSamplers
      }};
  InitVkDestroyer(LogicalDevice, rastDescriptorSetLayout);
  if (!CreateDescriptorSetLayout(*LogicalDevice, descriptor_set_layout_bindings,
                                 *rastDescriptorSetLayout)) {
    return false;
  }

  std::vector<VkDescriptorPoolSize> descriptor_pool_sizes = {
      {
          VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType     type
          1 // uint32_t             descriptorCount
      },
      {
          VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, // VkDescriptorType type
          1 // uint32_t             descriptorCount
      }};
  InitVkDestroyer(LogicalDevice, rastDescriptorPool);
  if (!CreateDescriptorPool(*LogicalDevice, false, 1, descriptor_pool_sizes,
                            *rastDescriptorPool)) {
    return false;
  }

  if (!AllocateDescriptorSets(*LogicalDevice, *rastDescriptorPool,
                              {*rastDescriptorSetLayout}, rastDescriptorSets)) {
    return false;
  }

  BufferDescriptorInfo buffer_descriptor_update = {
      rastDescriptorSets[0], // VkDescriptorSet TargetDescriptorSet
      0, // uint32_t                             TargetDescriptorBinding
      0, // uint32_t                             TargetArrayElement
      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType
      // TargetDescriptorType
      {// std::vector<VkDescriptorBufferInfo>  BufferInfos
       {
           *rastUniformBuffer, // VkBuffer                             buffer
           0,                  // VkDeviceSize                         offset
           VK_WHOLE_SIZE       // VkDeviceSize                         range
       }}};

  ImageDescriptorInfo image_descriptor_update = {
      rastDescriptorSets[0], // VkDescriptorSet TargetDescriptorSet
      1, // uint32_t                             TargetDescriptorBinding
      0, // uint32_t                             TargetArrayElement
      VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, // VkDescriptorType
      // TargetDescriptorType
      {// std::vector<VkDescriptorImageInfo>   ImageInfos
       {
           *rastShadowMapSampler, // VkSampler sampler
           *rastShadowMap.View,   // VkImageView imageView
           VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL // VkImageLayout
                                                           // imageLayout
       }}};

  UpdateDescriptorSets(*LogicalDevice, {image_descriptor_update},
                       {buffer_descriptor_update}, {}, {});

  // Shadow map render pass - for rendering into depth attachment

  std::vector<VkAttachmentDescription> shadow_map_attachment_descriptions = {{
      0,                            // VkAttachmentDescriptionFlags     flags
      DepthFormat,                  // VkFormat                         format
      VK_SAMPLE_COUNT_1_BIT,        // VkSampleCountFlagBits            samples
      VK_ATTACHMENT_LOAD_OP_CLEAR,  // VkAttachmentLoadOp               loadOp
      VK_ATTACHMENT_STORE_OP_STORE, // VkAttachmentStoreOp storeOp
      VK_ATTACHMENT_LOAD_OP_DONT_CARE,  // VkAttachmentLoadOp stencilLoadOp
      VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp stencilStoreOp
      VK_IMAGE_LAYOUT_UNDEFINED,        // VkImageLayout initialLayout
      VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL // VkImageLayout
                                                      // finalLayout
  }};

  VkAttachmentReference shadow_map_depth_attachment = {
      0, // uint32_t                             attachment
      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL // VkImageLayout layout
  };

  std::vector<SubpassParameters> shadow_map_subpass_parameters = {{
      VK_PIPELINE_BIND_POINT_GRAPHICS, // VkPipelineBindPoint PipelineType
      {}, // std::vector<VkAttachmentReference>   InputAttachments
      {}, // std::vector<VkAttachmentReference>   ColorAttachments
      {}, // std::vector<VkAttachmentReference>   ResolveAttachments
      &shadow_map_depth_attachment, // VkAttachmentReference const        *
      // DepthStencilAttachment
      {} // std::vector<uint32_t>                PreserveAttachments
  }};

  std::vector<VkSubpassDependency> shadow_map_subpass_dependencies = {
      {
          VK_SUBPASS_EXTERNAL, // uint32_t                   srcSubpass
          0,                   // uint32_t                   dstSubpass
          VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, // VkPipelineStageFlags
          // srcStageMask
          VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT, // VkPipelineStageFlags
          // dstStageMask
          VK_ACCESS_SHADER_READ_BIT, // VkAccessFlags srcAccessMask
          VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, // VkAccessFlags
          // dstAccessMask
          VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags dependencyFlags
      },
      {
          0,                   // uint32_t                   srcSubpass
          VK_SUBPASS_EXTERNAL, // uint32_t                   dstSubpass
          VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT, // VkPipelineStageFlags
          // srcStageMask
          VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, // VkPipelineStageFlags
          // dstStageMask
          VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, // VkAccessFlags
          // srcAccessMask
          VK_ACCESS_SHADER_READ_BIT,  // VkAccessFlags dstAccessMask
          VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags dependencyFlags
      }};

  InitVkDestroyer(LogicalDevice, rastShadowMapRenderPass);
  if (!CreateRenderPass(*LogicalDevice, shadow_map_attachment_descriptions,
                        shadow_map_subpass_parameters,
                        shadow_map_subpass_dependencies,
                        *rastShadowMapRenderPass)) {
    return false;
  }

  InitVkDestroyer(LogicalDevice, rastShadowMap.Framebuffer);
  if (!CreateFramebuffer(*LogicalDevice, *rastShadowMapRenderPass,
                         {*rastShadowMap.View}, 512, 512, 1,
                         *rastShadowMap.Framebuffer)) {
    return false;
  }

  // rastScene render pass - for rendering final scene

  std::vector<VkAttachmentDescription> rast_attachment_descriptions = {
      {
          0,                     // VkAttachmentDescriptionFlags     flags
          Swapchain.Format,      // VkFormat                         format
          VK_SAMPLE_COUNT_1_BIT, // VkSampleCountFlagBits            samples
          VK_ATTACHMENT_LOAD_OP_CLEAR,      // VkAttachmentLoadOp loadOp
          VK_ATTACHMENT_STORE_OP_STORE,     // VkAttachmentStoreOp storeOp
          VK_ATTACHMENT_LOAD_OP_DONT_CARE,  // VkAttachmentLoadOp stencilLoadOp
          VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp
          // stencilStoreOp
          VK_IMAGE_LAYOUT_UNDEFINED,      // VkImageLayout initialLayout
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR // VkImageLayout finalLayout
      },
      {
          0,                     // VkAttachmentDescriptionFlags     flags
          DepthFormat,           // VkFormat                         format
          VK_SAMPLE_COUNT_1_BIT, // VkSampleCountFlagBits            samples
          VK_ATTACHMENT_LOAD_OP_CLEAR,      // VkAttachmentLoadOp loadOp
          VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp storeOp
          VK_ATTACHMENT_LOAD_OP_DONT_CARE,  // VkAttachmentLoadOp stencilLoadOp
          VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp
          // stencilStoreOp
          VK_IMAGE_LAYOUT_UNDEFINED, // VkImageLayout initialLayout
          VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL // VkImageLayout
                                                           // finalLayout
      }};

  VkAttachmentReference depth_attachment = {
      1, // uint32_t                             attachment
      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL // VkImageLayout
                                                       // layout;
  };

  std::vector<SubpassParameters> rast_subpass_parameters = {{
      VK_PIPELINE_BIND_POINT_GRAPHICS, // VkPipelineBindPoint PipelineType
      {}, // std::vector<VkAttachmentReference>   InputAttachments
      {   // std::vector<VkAttachmentReference>   ColorAttachments
       {
           0, // uint32_t                             attachment
           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // VkImageLayout layout
       }},
      {}, // std::vector<VkAttachmentReference>   ResolveAttachments
      &depth_attachment, // VkAttachmentReference const        *
      // DepthStencilAttachment
      {} // std::vector<uint32_t>                PreserveAttachments
  }};

  std::vector<VkSubpassDependency> subpass_dependencies = {
      {
          VK_SUBPASS_EXTERNAL, // uint32_t                   srcSubpass
          0,                   // uint32_t                   dstSubpass
          VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT, // VkPipelineStageFlags
          // srcStageMask
          VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, // VkPipelineStageFlags
          // dstStageMask
          VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, // VkAccessFlags
          // srcAccessMask
          VK_ACCESS_SHADER_READ_BIT,  // VkAccessFlags dstAccessMask
          VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags dependencyFlags
      },
      {
          0,                   // uint32_t                   srcSubpass
          VK_SUBPASS_EXTERNAL, // uint32_t                   dstSubpass
          VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // VkPipelineStageFlags
          // srcStageMask
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // VkPipelineStageFlags
          // dstStageMask
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags srcAccessMask
          VK_ACCESS_MEMORY_READ_BIT,            // VkAccessFlags dstAccessMask
          VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags dependencyFlags
      }};

  InitVkDestroyer(LogicalDevice, rastSceneRenderPass);
  if (!CreateRenderPass(*LogicalDevice, rast_attachment_descriptions,
                        rast_subpass_parameters, subpass_dependencies,
                        *rastSceneRenderPass)) {
    return false;
  }

  // Graphics pipeline

  std::vector<VkPushConstantRange> push_constant_ranges = {{
      VK_SHADER_STAGE_VERTEX_BIT, // VkShaderStageFlags     stageFlags
      0,                          // uint32_t               offset
      sizeof(float) * 4           // uint32_t               size
  }};
  InitVkDestroyer(LogicalDevice, rastPipelineLayout);
  if (!CreatePipelineLayout(*LogicalDevice, {*rastDescriptorSetLayout},
                            push_constant_ranges, *rastPipelineLayout)) {
    return false;
  }

  // rastScene

  std::vector<unsigned char> scene_vertex_shader_spirv;
  if (!GetBinaryFileContents("Data/Shaders/rasterization.vert",
                             scene_vertex_shader_spirv)) {
    return false;
  }

  VkDestroyer(VkShaderModule) scene_vertex_shader_module;
  InitVkDestroyer(LogicalDevice, scene_vertex_shader_module);
  if (!CreateShaderModule(*LogicalDevice, scene_vertex_shader_spirv,
                          *scene_vertex_shader_module)) {
    return false;
  }

  std::vector<unsigned char> scene_fragment_shader_spirv;
  if (!GetBinaryFileContents("Data/Shaders/rasterization.frag",
                             scene_fragment_shader_spirv)) {
    return false;
  }
  VkDestroyer(VkShaderModule) scene_fragment_shader_module;
  InitVkDestroyer(LogicalDevice, scene_fragment_shader_module);
  if (!CreateShaderModule(*LogicalDevice, scene_fragment_shader_spirv,
                          *scene_fragment_shader_module)) {
    return false;
  }

  std::vector<ShaderStageParameters> scene_shader_stage_params = {
      {
          VK_SHADER_STAGE_VERTEX_BIT,  // VkShaderStageFlagBits ShaderStage
          *scene_vertex_shader_module, // VkShaderModule ShaderModule
          "main", // char const                 * EntryPointName;
          nullptr // VkSpecializationInfo const * SpecializationInfo;
      },
      {
          VK_SHADER_STAGE_FRAGMENT_BIT,  // VkShaderStageFlagBits ShaderStage
          *scene_fragment_shader_module, // VkShaderModule ShaderModule
          "main", // char const                 * EntryPointName
          nullptr // VkSpecializationInfo const * SpecializationInfo
      }};

  std::vector<VkPipelineShaderStageCreateInfo> scene_shader_stage_create_infos;
  SpecifyPipelineShaderStages(scene_shader_stage_params,
                              scene_shader_stage_create_infos);

  std::vector<VkVertexInputBindingDescription>
      scene_vertex_input_binding_descriptions = {{
          0,                          // uint32_t                     binding
          6 * sizeof(float),          // uint32_t                     stride
          VK_VERTEX_INPUT_RATE_VERTEX // VkVertexInputRate inputRate
      }};

  std::vector<VkVertexInputAttributeDescription>
      scene_vertex_attribute_descriptions = {
          {
              0,                          // uint32_t   location
              0,                          // uint32_t   binding
              VK_FORMAT_R32G32B32_SFLOAT, // VkFormat   format
              0                           // uint32_t   offset
          },
          {
              1,                          // uint32_t   location
              0,                          // uint32_t   binding
              VK_FORMAT_R32G32B32_SFLOAT, // VkFormat   format
              3 * sizeof(float)           // uint32_t   offset
          }};

  VkPipelineVertexInputStateCreateInfo scene_vertex_input_state_create_info;
  SpecifyPipelineVertexInputState(scene_vertex_input_binding_descriptions,
                                  scene_vertex_attribute_descriptions,
                                  scene_vertex_input_state_create_info);

  // Shadow

  std::vector<unsigned char> shadow_vertex_shader_spirv;
  if (!GetBinaryFileContents("Data/Shaders/shadow.vert",
                             shadow_vertex_shader_spirv)) {
    return false;
  }

  VkDestroyer(VkShaderModule) shadow_vertex_shader_module;
  InitVkDestroyer(LogicalDevice, shadow_vertex_shader_module);
  if (!CreateShaderModule(*LogicalDevice, shadow_vertex_shader_spirv,
                          *shadow_vertex_shader_module)) {
    return false;
  }

  std::vector<ShaderStageParameters> shadow_shader_stage_params = {{
      VK_SHADER_STAGE_VERTEX_BIT,   // VkShaderStageFlagBits        ShaderStage
      *shadow_vertex_shader_module, // VkShaderModule ShaderModule
      "main", // char const                 * EntryPointName;
      nullptr // VkSpecializationInfo const * SpecializationInfo;
  }};

  std::vector<VkPipelineShaderStageCreateInfo> shadow_shader_stage_create_infos;
  SpecifyPipelineShaderStages(shadow_shader_stage_params,
                              shadow_shader_stage_create_infos);

  std::vector<VkVertexInputBindingDescription>
      shadow_vertex_input_binding_descriptions = {{0,6 * sizeof(float),VK_VERTEX_INPUT_RATE_VERTEX}};
  std::vector<VkVertexInputAttributeDescription>
      shadow_vertex_attribute_descriptions = {
          {0,0,VK_FORMAT_R32G32B32_SFLOAT,0},
      };

  VkPipelineVertexInputStateCreateInfo shadow_vertex_input_state_create_info;
  SpecifyPipelineVertexInputState(shadow_vertex_input_binding_descriptions,
                                  shadow_vertex_attribute_descriptions,
                                  shadow_vertex_input_state_create_info);

  // Common
  VkPipelineInputAssemblyStateCreateInfo rast_input_assembly_state_create_info;
  SpecifyPipelineInputAssemblyState(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, false,
                                    rast_input_assembly_state_create_info);

  VkPipelineViewportStateCreateInfo rast_viewport_state_create_info;
  SpecifyPipelineViewportAndScissorTestState(viewport_infos,
                                             rast_viewport_state_create_info);

  VkPipelineRasterizationStateCreateInfo rast_rasterization_state_create_info;
  SpecifyPipelineRasterizationState(
      false, false, VK_POLYGON_MODE_FILL, VK_CULL_MODE_BACK_BIT,
      VK_FRONT_FACE_COUNTER_CLOCKWISE, false, 0.0f, 0.0f, 0.0f, 1.0f,
      rast_rasterization_state_create_info);

  VkPipelineMultisampleStateCreateInfo rast_multisample_state_create_info;
  SpecifyPipelineMultisampleState(VK_SAMPLE_COUNT_1_BIT, false, 0.0f, nullptr,
                                  false, false,
                                  rast_multisample_state_create_info);

  VkPipelineDepthStencilStateCreateInfo depth_stencil_state_create_info;
  SpecifyPipelineDepthAndStencilState(true, true, VK_COMPARE_OP_LESS_OR_EQUAL,
                                      false, 0.0f, 1.0f, false, {}, {},
                                      depth_stencil_state_create_info);

  std::vector<VkPipelineColorBlendAttachmentState>
      rast_attachment_blend_states = {
          {false,               // VkBool32                 blendEnable
           VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcColorBlendFactor
           VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstColorBlendFactor
           VK_BLEND_OP_ADD,     // VkBlendOp                colorBlendOp
           VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcAlphaBlendFactor
           VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstAlphaBlendFactor
           VK_BLEND_OP_ADD,     // VkBlendOp                alphaBlendOp
           VK_COLOR_COMPONENT_R_BIT | // VkColorComponentFlags    colorWriteMask
               VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
               VK_COLOR_COMPONENT_A_BIT}};
  VkPipelineColorBlendStateCreateInfo rast_blend_state_create_info;
  SpecifyPipelineBlendState(
      false, VK_LOGIC_OP_COPY, rast_attachment_blend_states,
      {1.0f, 1.0f, 1.0f, 1.0f}, rast_blend_state_create_info);

  // rastScene

  std::vector<VkDynamicState> rast_dynamic_states = {VK_DYNAMIC_STATE_VIEWPORT,
                                                     VK_DYNAMIC_STATE_SCISSOR};
  VkPipelineDynamicStateCreateInfo rast_dynamic_state_create_info;
  SpecifyPipelineDynamicStates(rast_dynamic_states,
                               rast_dynamic_state_create_info);

  VkGraphicsPipelineCreateInfo scene_pipeline_create_info;
  SpecifyGraphicsPipelineCreationParameters(
      0, scene_shader_stage_create_infos, scene_vertex_input_state_create_info,
      rast_input_assembly_state_create_info, nullptr,
      &rast_viewport_state_create_info, rast_rasterization_state_create_info,
      &rast_multisample_state_create_info, &depth_stencil_state_create_info,
      &rast_blend_state_create_info, &rast_dynamic_state_create_info,
      *rastPipelineLayout, *rastSceneRenderPass, 0, VK_NULL_HANDLE, -1,
      scene_pipeline_create_info);

  std::vector<VkPipeline> scene_pipeline;
  if (!CreateGraphicsPipelines(*LogicalDevice, {scene_pipeline_create_info},
                               VK_NULL_HANDLE, scene_pipeline)) {
    return false;
  }
  InitVkDestroyer(LogicalDevice, rastScenePipeline);
  *rastScenePipeline = scene_pipeline[0];

  // Shadow

  VkGraphicsPipelineCreateInfo shadow_pipeline_create_info;
  SpecifyGraphicsPipelineCreationParameters(
      0, shadow_shader_stage_create_infos,
      shadow_vertex_input_state_create_info,
      rast_input_assembly_state_create_info, nullptr,
      &rast_viewport_state_create_info, rast_rasterization_state_create_info,
      &rast_multisample_state_create_info, &depth_stencil_state_create_info,
      nullptr, nullptr, *rastPipelineLayout, *rastShadowMapRenderPass, 0,
      VK_NULL_HANDLE, -1, shadow_pipeline_create_info);

  std::vector<VkPipeline> shadow_pipeline;
  if (!CreateGraphicsPipelines(*LogicalDevice, {shadow_pipeline_create_info},
                               VK_NULL_HANDLE, shadow_pipeline)) {
    return false;
  }
  InitVkDestroyer(LogicalDevice, rastShadowMapPipeline);
  *rastShadowMapPipeline = shadow_pipeline[0];

  return true;
}

bool MutualDemo::initFramerateLayerPipeline() {
  std::vector<float> vertices = {-.95, -1., 0., 1., -.95, -.7, 0., 0.,
                                 -.65, -1., 1., 1., -.65, -1., 1., 1.,
                                 -.95, -.7, 0., 0., -.65, -.7, 1., 0.};

  InitVkDestroyer(LogicalDevice, frVertexBuffer);
  if (!CreateBuffer(*LogicalDevice, sizeof(vertices[0]) * vertices.size(),
                    VK_BUFFER_USAGE_TRANSFER_DST_BIT |
                        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                    *frVertexBuffer)) {
    return false;
  }

  InitVkDestroyer(LogicalDevice, frVertexBufferMemory);
  if (!AllocateAndBindMemoryObjectToBuffer(
          PhysicalDevice, *LogicalDevice, *frVertexBuffer,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, *frVertexBufferMemory)) {
    return false;
  }

  if (!UseStagingBufferToUpdateBufferWithDeviceLocalMemoryBound(
          PhysicalDevice, *LogicalDevice, sizeof(vertices[0]) * vertices.size(),
          &vertices[0], *frVertexBuffer, 0, 0,
          VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
          GraphicsQueue.Handle, FramesResources.front().CommandBuffer, {})) {
    return false;
  }

  // Render pass
  std::vector<VkAttachmentDescription> attachment_descriptions = {{
      0,                     // VkAttachmentDescriptionFlags     flags
      Swapchain.Format,      // VkFormat                         format
      VK_SAMPLE_COUNT_1_BIT, // VkSampleCountFlagBits            samples
      VK_ATTACHMENT_LOAD_OP_DONT_CARE,  // VkAttachmentLoadOp loadOp
      VK_ATTACHMENT_STORE_OP_STORE,     // VkAttachmentStoreOp storeOp
      VK_ATTACHMENT_LOAD_OP_DONT_CARE,  // VkAttachmentLoadOp stencilLoadOp
      VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp stencilStoreOp
      VK_IMAGE_LAYOUT_UNDEFINED,        // VkImageLayout initialLayout
      VK_IMAGE_LAYOUT_PRESENT_SRC_KHR   // VkImageLayout finalLayout
  }};

  std::vector<SubpassParameters> subpass_parameters = {{
      VK_PIPELINE_BIND_POINT_GRAPHICS, // VkPipelineBindPoint PipelineType
      {}, // std::vector<VkAttachmentReference>   InputAttachments
      {   // std::vector<VkAttachmentReference>   ColorAttachments
       {
           0, // uint32_t                             attachment
           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // VkImageLayout layout
       }},
      {}, // std::vector<VkAttachmentReference>   ResolveAttachments
      {}, // VkAttachmentReference const        * DepthStencilAttachment
      {}  // std::vector<uint32_t>                PreserveAttachments
  }};

  std::vector<VkSubpassDependency> subpass_dependencies = {
      {
          VK_SUBPASS_EXTERNAL, // uint32_t                   srcSubpass
          0,                   // uint32_t                   dstSubpass
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // VkPipelineStageFlags
          // srcStageMask
          VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // VkPipelineStageFlags
          // dstStageMask
          VK_ACCESS_MEMORY_READ_BIT,            // VkAccessFlags srcAccessMask
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags dstAccessMask
          VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags dependencyFlags
      },
      {
          0,                   // uint32_t                   srcSubpass
          VK_SUBPASS_EXTERNAL, // uint32_t                   dstSubpass
          VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // VkPipelineStageFlags
          // srcStageMask
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // VkPipelineStageFlags
          // dstStageMask
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags srcAccessMask
          VK_ACCESS_MEMORY_READ_BIT,            // VkAccessFlags dstAccessMask
          VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags dependencyFlags
      }};

  InitVkDestroyer(LogicalDevice, frRenderPass);
  if (!CreateRenderPass(*LogicalDevice, attachment_descriptions,
                        subpass_parameters, subpass_dependencies,
                        *frRenderPass)) {
    return false;
  }

  // Graphics pipeline

  std::vector<VkPushConstantRange> push_constant_ranges = {
      {VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(float)}};
  InitVkDestroyer(LogicalDevice, frPipelineLayout);
  if (!CreatePipelineLayout(*LogicalDevice, {}, {push_constant_ranges},
                            *frPipelineLayout)) {
    return false;
  }

  std::vector<unsigned char> vertex_shader_spirv;
  if (!GetBinaryFileContents("Data/Shaders/framerate.vert",
                             vertex_shader_spirv)) {
    return false;
  }

  VkDestroyer(VkShaderModule) vertex_shader_module;
  InitVkDestroyer(LogicalDevice, vertex_shader_module);
  if (!CreateShaderModule(*LogicalDevice, vertex_shader_spirv,
                          *vertex_shader_module)) {
    return false;
  }

  std::vector<unsigned char> fragment_shader_spirv;
  if (!GetBinaryFileContents("Data/Shaders/framerate.frag",
                             fragment_shader_spirv)) {
    return false;
  }
  VkDestroyer(VkShaderModule) fragment_shader_module;
  InitVkDestroyer(LogicalDevice, fragment_shader_module);
  if (!CreateShaderModule(*LogicalDevice, fragment_shader_spirv,
                          *fragment_shader_module)) {
    return false;
  }

  std::vector<ShaderStageParameters> shader_stage_params = {
      {VK_SHADER_STAGE_VERTEX_BIT, *vertex_shader_module, "main", nullptr},
      {VK_SHADER_STAGE_FRAGMENT_BIT, *fragment_shader_module, "main", nullptr}};
  std::vector<VkPipelineShaderStageCreateInfo> shader_stage_create_infos;
  SpecifyPipelineShaderStages(shader_stage_params, shader_stage_create_infos);
  std::vector<VkVertexInputAttributeDescription> vertex_attribute_descriptions =
      {{0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0},
       {1, 0, VK_FORMAT_R32G32B32_SFLOAT, 2 * sizeof(float)}};
  std::vector<VkVertexInputBindingDescription>
      vertex_input_binding_descriptions = {
          {0, 4 * sizeof(float), VK_VERTEX_INPUT_RATE_VERTEX}};

  VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info;
  SpecifyPipelineVertexInputState(vertex_input_binding_descriptions,
                                  vertex_attribute_descriptions,
                                  vertex_input_state_create_info);

  VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info;
  SpecifyPipelineInputAssemblyState(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, false,
                                    input_assembly_state_create_info);

  VkPipelineViewportStateCreateInfo viewport_state_create_info;
  SpecifyPipelineViewportAndScissorTestState(viewport_infos,
                                             viewport_state_create_info);

  VkPipelineRasterizationStateCreateInfo rasterization_state_create_info;
  SpecifyPipelineRasterizationState(
      false, false, VK_POLYGON_MODE_FILL, VK_CULL_MODE_BACK_BIT,
      VK_FRONT_FACE_COUNTER_CLOCKWISE, false, 0.0f, 0.0f, 0.0f, 1.0f,
      rasterization_state_create_info);

  VkPipelineMultisampleStateCreateInfo multisample_state_create_info;
  SpecifyPipelineMultisampleState(VK_SAMPLE_COUNT_1_BIT, false, 0.0f, nullptr,
                                  false, false, multisample_state_create_info);

  VkPipelineDepthStencilStateCreateInfo depth_stencil_state_create_info;
  SpecifyPipelineDepthAndStencilState(false, false, VK_COMPARE_OP_ALWAYS, false,
                                      0.0f, 1.0f, false, {}, {},
                                      depth_stencil_state_create_info);

  std::vector<VkPipelineColorBlendAttachmentState> attachment_blend_states = {
      {true,                // VkBool32                 blendEnable
       VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcColorBlendFactor
       VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstColorBlendFactor
       VK_BLEND_OP_REVERSE_SUBTRACT, // VkBlendOp                colorBlendOp
       VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcAlphaBlendFactor
       VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstAlphaBlendFactor
       VK_BLEND_OP_ADD,     // VkBlendOp                alphaBlendOp
       VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
           VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT}};
  VkPipelineColorBlendStateCreateInfo blend_state_create_info;
  SpecifyPipelineBlendState(false, VK_LOGIC_OP_COPY, attachment_blend_states,
                            {1.0f, 1.0f, 1.0f, 1.0f}, blend_state_create_info);

  std::vector<VkDynamicState> dynamic_states = {VK_DYNAMIC_STATE_VIEWPORT,
                                                VK_DYNAMIC_STATE_SCISSOR};
  VkPipelineDynamicStateCreateInfo dynamic_state_create_info;
  SpecifyPipelineDynamicStates(dynamic_states, dynamic_state_create_info);

  VkGraphicsPipelineCreateInfo pipeline_create_info;
  SpecifyGraphicsPipelineCreationParameters(
      0, shader_stage_create_infos, vertex_input_state_create_info,
      input_assembly_state_create_info, nullptr, &viewport_state_create_info,
      rasterization_state_create_info, &multisample_state_create_info,
      &depth_stencil_state_create_info, &blend_state_create_info,
      &dynamic_state_create_info, *frPipelineLayout, *frRenderPass, 0,
      VK_NULL_HANDLE, -1, pipeline_create_info);

  std::vector<VkPipeline> pipeline;
  if (!CreateGraphicsPipelines(*LogicalDevice, {pipeline_create_info},
                               VK_NULL_HANDLE, pipeline)) {
    return false;
  }
  InitVkDestroyer(LogicalDevice, frPipeline);
  *frPipeline = pipeline[0];
  return true;
}
bool MutualDemo::renderFrameRateLayer() {
  fpsCounter.appendFrame(TimerState.GetDeltaTime());
  if(std::chrono::duration_cast<std::chrono::milliseconds>(TimerState.GetTime() - fpsReportTime).count() > 1000){
    fpsReportTime = TimerState.GetTime();
    fpsToDisplay = 1.f / fpsCounter.average();
  }

  auto prepare_frame = [&](VkCommandBuffer command_buffer,
                           uint32_t swapchain_image_index,
                           VkFramebuffer framebuffer) {
    if (!BeginCommandBufferRecordingOperation(
            command_buffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
            nullptr)) {
      return false;
    }

    if (PresentQueue.FamilyIndex != GraphicsQueue.FamilyIndex) {
      ImageTransition image_transition_before_drawing = {
          Swapchain.Images[swapchain_image_index], // VkImage Image
          VK_ACCESS_MEMORY_READ_BIT, // VkAccessFlags        CurrentAccess
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags NewAccess
          VK_IMAGE_LAYOUT_UNDEFINED, // VkImageLayout        CurrentLayout
          VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // VkImageLayout NewLayout
          PresentQueue.FamilyIndex,  // uint32_t             CurrentQueueFamily
          GraphicsQueue.FamilyIndex, // uint32_t             NewQueueFamily
          VK_IMAGE_ASPECT_COLOR_BIT  // VkImageAspectFlags   Aspect
      };
      SetImageMemoryBarrier(command_buffer,
                            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                            {image_transition_before_drawing});
    }

    // Drawing
    BeginRenderPass(
        command_buffer, *frRenderPass, framebuffer, {{0, 0}, Swapchain.Size},
        {{0.1f, 0.2f, 0.3f, 1.0f}, {1.0f, 0}}, VK_SUBPASS_CONTENTS_INLINE);

    VkViewport viewport = {0.0f,
                           0.0f,
                           static_cast<float>(Swapchain.Size.width),
                           static_cast<float>(Swapchain.Size.height),
                           0.0f,
                           1.0f};
    SetViewportStateDynamically(command_buffer, 0, {viewport});
    VkRect2D scissor = {{0, 0}, {Swapchain.Size.width, Swapchain.Size.height}};
    SetScissorStateDynamically(command_buffer, 0, {scissor});
    BindVertexBuffers(command_buffer, 0, {{*frVertexBuffer, 0}});
    BindPipelineObject(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                       *frPipeline);
    ProvideDataToShadersThroughPushConstants(command_buffer, *frPipelineLayout,
                                             VK_SHADER_STAGE_FRAGMENT_BIT, 0,
                                             sizeof(float), &fpsToDisplay);
    DrawGeometry(command_buffer, 6, 1, 0, 0);
    EndRenderPass(command_buffer);

    if (PresentQueue.FamilyIndex != GraphicsQueue.FamilyIndex) {
      ImageTransition image_transition_before_present = {
          Swapchain.Images[swapchain_image_index], // VkImage Image
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags CurrentAccess
          VK_ACCESS_MEMORY_READ_BIT,       // VkAccessFlags        NewAccess
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // VkImageLayout CurrentLayout
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // VkImageLayout        NewLayout
          GraphicsQueue.FamilyIndex,       // uint32_t CurrentQueueFamily
          PresentQueue.FamilyIndex, // uint32_t             NewQueueFamily
          VK_IMAGE_ASPECT_COLOR_BIT // VkImageAspectFlags   Aspect
      };
      SetImageMemoryBarrier(command_buffer,
                            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                            VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                            {image_transition_before_present});
    }

    return EndCommandBufferRecordingOperation(command_buffer);
  };

  bool res =
      IncreasePerformanceThroughIncreasingTheNumberOfSeparatelyRenderedFrames(
          *LogicalDevice, GraphicsQueue.Handle, PresentQueue.Handle,
          *Swapchain.Handle, Swapchain.Size, Swapchain.ImageViewsRaw,
          *frRenderPass, {}, prepare_frame, FramesResources);

  return res;
}
