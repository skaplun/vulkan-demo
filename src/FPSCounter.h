#ifndef VULKAN_RAYTRACER_FPSCOUNTER_H
#define VULKAN_RAYTRACER_FPSCOUNTER_H

//#include <queue>
#include <list>

class FPSCounter {
public:
  void appendFrame(float timeDelta);
  float average();
private:
  static const int MAX_ITEMS = 20;
  std::list<float> timeDeltas;
};

#endif // VULKAN_RAYTRACER_FPSCOUNTER_H
