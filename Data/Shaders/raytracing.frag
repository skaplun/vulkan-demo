#version 450

#define SURFACE_TYPE_NONE -1
#define SURFACE_TYPE_PLANE 1
#define SURFACE_TYPE_SPHERE 2

layout( location = 0 ) in vec2 st;
layout( location = 0 ) out vec4 frag_color;

layout( set = 0, binding = 0 ) uniform UniformBuffer {
    vec3 CameraPos;
    vec3 LightPos;
};

struct Sphere{ vec3 origin; float rad; };
struct Plane{ vec3 origin; vec3 normal; };
struct Ray{ vec3 origin, dir; };
struct HitRecord{ float t; vec3 p; vec3 normal; int surfaceType; };

bool sphere_hit(in Sphere sphere, in Ray ray, inout HitRecord rec){
    vec3 oc = ray.origin - sphere.origin;
    float b = dot( oc, ray.dir );
    float c = dot( oc, oc ) - sphere.rad * sphere.rad;
    float h = b * b - c;
    if(h < 0.) return false;
    rec.t = -b - sqrt( h );
    rec.p = ray.origin + ray.dir * rec.t;
    rec.normal = (rec.p - sphere.origin) / sphere.rad;
    return true;
}

bool plane_hit( in Ray ray, in Plane plane, inout HitRecord rec)
{
    float t = (plane.origin.y - ray.origin.y)/ray.dir.y;
    if(t > 0.){
        rec.t = t;
        rec.p = ray.origin + ray.dir * rec.t;
        rec.normal = plane.normal;
        return true;
    }else
        return false;
}

mat3 viewMatrix(vec3 eye, vec3 center, vec3 up) {
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(f, s);
    return mat3(s, u, -f);
}

float sphSoftShadow(in vec3 ro, in vec3 rd, in vec4 sph, in float k){
    vec3 oc = ro - sph.xyz;
    float b = dot(oc, rd);
    float c = dot(oc, oc) - sph.w*sph.w;
    float h = b*b - c;
    float d = sqrt(max(0.0, sph.w*sph.w-h)) - sph.w;
    float t = -b - sqrt(max(h, 0.0));
    return (t<0.0) ? 1.0 : smoothstep(0.0, 1.0, 2.5*k*d/t);
}

const vec2 RESOLUTION = vec2(1280., 800.);
const float FOV = 90.;
const vec3 LIGHT_POS = vec3(-6., 4., 3.4641);

vec4 render(vec2 coord) {
    //vec3 eye = vec3(3., 1., 5.);
    //vec3 eye = cp;
    vec3 eye = CameraPos;
    vec2 xy = coord - RESOLUTION / 2.0;
    float z = RESOLUTION.y / tan(radians(FOV) / 2.0);
    vec3 viewDir = normalize(vec3(xy, -z));
    vec3 worldDir = mat3(viewMatrix(eye, vec3(0., -1., 0.), vec3(0., 1., 0.))) * viewDir;

    HitRecord finalHitRecord = HitRecord(1e10, vec3(0.), vec3(0.), SURFACE_TYPE_NONE);
    HitRecord tmpHitRecord;
    int surfaceType = 0;
    if (plane_hit(Ray(eye, worldDir), Plane(vec3(0., -1.5, 0.), vec3(0., 1., 0.)), tmpHitRecord)){
        if (max(tmpHitRecord.p.x, tmpHitRecord.p.z) < 2. &&
        min(tmpHitRecord.p.x, tmpHitRecord.p.z) > -2.){
            finalHitRecord = tmpHitRecord;
            finalHitRecord.surfaceType = SURFACE_TYPE_PLANE;
        }
    }
    if (sphere_hit(Sphere(vec3(0.), 1.), Ray(eye, worldDir), tmpHitRecord) && tmpHitRecord.t < finalHitRecord.t){
        finalHitRecord = tmpHitRecord;
        finalHitRecord.surfaceType = SURFACE_TYPE_SPHERE;
    }
    vec3 color = vec3(.1, .2, .3);
    if (finalHitRecord.surfaceType != SURFACE_TYPE_NONE){
        vec3 toLight = normalize(LIGHT_POS - finalHitRecord.p);
        color = vec3(clamp(dot(toLight, normalize(finalHitRecord.normal)), .1, 1.));

        if (finalHitRecord.surfaceType == SURFACE_TYPE_PLANE)
            color *= sphSoftShadow(finalHitRecord.p, toLight, vec4(vec3(0.), 1.), 5.);
    }

    return vec4(color, 1.);
}

#define AA 1
void main(){
    frag_color = vec4(0.);
    for(int y = 0; y < AA; ++y)
    for(int x = 0; x < AA; ++x){
        frag_color += clamp(render(gl_FragCoord.xy + vec2(x, y) / float(AA)), 0., 1.);
    }
    frag_color.rgb /= float(AA * AA);
}