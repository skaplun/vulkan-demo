#ifndef VULKAN_RAYTRACER_MUTUALDEMO_H
#define VULKAN_RAYTRACER_MUTUALDEMO_H

#include "CookbookSampleFramework.h"
#include "OrbitingCamera.h"
#include "FPSCounter.h"

using namespace VulkanCookbook;
class MutualDemo : public VulkanCookbookSample {
public:
  bool Initialize(WindowParameters window_parameters) override;
  bool Draw() override;
  bool Resize() override;
private:
  void OnMouseEvent(MouseStateParameters &me) override;
  void SwitchMode() override;
  bool UpdateStagingBuffer(bool);
  bool renderRasterizer();
  bool renderRaytracer();
  bool renderFrameRateLayer();
  bool initRaytracePipeline();
  bool initRasterizePipeline();
  bool initFramerateLayerPipeline();

  VkDestroyer(VkBuffer) rtVertexBuffer;
  VkDestroyer(VkDeviceMemory) rtVertexBufferMemory;
  VkDestroyer(VkDescriptorSetLayout) rtDescriptorSetLayout;
  VkDestroyer(VkDescriptorPool) rtDescriptorPool;
  std::vector<VkDescriptorSet> rtDescriptorSets;
  VkDestroyer(VkRenderPass) rtRenderPass;
  VkDestroyer(VkPipelineLayout) rtPipelineLayout;
  VkDestroyer(VkPipeline) rtPipeline;
  VkDestroyer(VkBuffer) rtStagingBuffer;
  VkDestroyer(VkDeviceMemory) rtStagingBufferMemory;
  bool rtUpdateUniformBuffer;
  VkDestroyer(VkBuffer) rtUniformBuffer;
  VkDestroyer(VkDeviceMemory) rtUniformBufferMemory;

  std::array<Mesh, 2> rastScene;
  VkDestroyer(VkBuffer) rastVertexBuffer;
  VkDestroyer(VkDeviceMemory) rastVertexBufferMemory;

  struct ShadowMapParameters {
    VkDestroyer(VkImage) Image;
    VkDestroyer(VkDeviceMemory) Memory;
    VkDestroyer(VkImageView) View;
    VkDestroyer(VkFramebuffer) Framebuffer;
  } rastShadowMap;
  VkDestroyer(VkSampler) rastShadowMapSampler;

  VkDestroyer(VkDescriptorSetLayout) rastDescriptorSetLayout;
  VkDestroyer(VkDescriptorPool) rastDescriptorPool;
  std::vector<VkDescriptorSet> rastDescriptorSets;

  VkDestroyer(VkPipelineLayout) rastPipelineLayout;

  VkDestroyer(VkRenderPass) rastShadowMapRenderPass;
  VkDestroyer(VkPipeline) rastShadowMapPipeline;
  VkDestroyer(VkRenderPass) rastSceneRenderPass;
  VkDestroyer(VkPipeline) rastScenePipeline;

  VkDestroyer(VkBuffer) rastStagingBuffer;
  VkDestroyer(VkDeviceMemory) rastStagingBufferMemory;
  bool rastUpdateUniformBuffer;
  VkDestroyer(VkBuffer) rastUniformBuffer;
  VkDestroyer(VkDeviceMemory) rastUniformBufferMemory;

  OrbitingCamera LightSource;
  OrbitingCamera Camera;
  bool m_rndRaytracer;
  FPSCounter fpsCounter;
  float fpsToDisplay;
  std::chrono::time_point<std::chrono::high_resolution_clock> fpsReportTime;

  VkDestroyer(VkBuffer) frVertexBuffer;
  VkDestroyer(VkDeviceMemory) frVertexBufferMemory;
  VkDestroyer(VkRenderPass) frRenderPass;
  VkDestroyer(VkPipelineLayout) frPipelineLayout;
  VkDestroyer(VkPipeline) frPipeline;
};

#endif // VULKAN_RAYTRACER_MUTUALDEMO_H
