#version 450

layout( location = 0 ) in vec2 p;
layout( location = 1 ) in vec2 st;

layout( location = 0 ) out vec2 out_st;
void main() {
    gl_Position = vec4(p, 0., 1.);
    out_st = st;
}