#include "CookbookSampleFramework.h"
#include "MutualDemo.h"

VULKAN_COOKBOOK_SAMPLE_FRAMEWORK("Rasterization/Raytracing demo", 50, 25, 1280,
                                 800, MutualDemo)