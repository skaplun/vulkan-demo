#version 450

layout( location = 0 ) in vec2 st;
layout( location = 0 ) out vec4 frag_color;

layout(push_constant) uniform FPSParameters {
    float fps;
} FPS;

float ddig(vec2 uv, int dig){

    bool u=true,
    c=false,
    l=false,
    t=true;

    if(uv.y<min(0.,abs(uv.x)-0.5)) u=!u;
    uv.y=abs(uv.y)-0.5;
    if(abs(uv.x)<abs(uv.y)){ uv=uv.yx; c=!c; }
    uv.y=abs(uv.y)-0.4;
    if(uv.x<0.) l=!l;
    uv.x=abs(abs(uv.x)-0.5);

    dig-=(dig/10)*10;

    if((dig==0) && (c&&l)) return 1.;
    else if((dig==1) && (c||l)) return 1.;
    else if((dig==2) && ((u&&l||!(u||l))&&!c)) return 1.;
    else if((dig==3) && (l&&!c)) return 1.;
    else if((dig==4) && (c&&!l||l&&!u)) return 1.;
    else if((dig==5) && (!c &&(!l&&u || l&&!u))) return 1.;
    else if((dig==6) && (u&&!c&&!l)) return 1.;
    else if((dig==7) && (l||c&&!u)) return 1.;
    else if((dig==9) && (!u&&l)) return 1.;

    return uv.x+max(0.,uv.y);
}

vec3 shade(vec2 uv, float val){
    float v = 1.;
    for(float i=0. ; i<3. ; i++){
        float d = val * pow(.1, i);
        v = min(v, ddig(uv * 1.5 + vec2(i * 1.3 - .35,0.), int(d)));
    }
    v = min(v, ddig(uv * 1.5 + vec2(-2.25,0.), int(val * 10.)));
    v = min(v, smoothstep(.05, .1, distance(uv, vec2(.875, -.6))));

    v=smoothstep(0.07,0.05,v);

    return vec3(v);
}

void main(){
    vec2 uv = 2. * st - 1.;
    frag_color = vec4(shade(uv * 2., FPS.fps), 1.0);
}