#include "FPSCounter.h"
#include <numeric>

void FPSCounter::appendFrame(float timeDelta) {
  if(timeDeltas.size() >= MAX_ITEMS)
    timeDeltas.pop_front();
  timeDeltas.push_back(timeDelta);
}

float FPSCounter::average() {
  if(timeDeltas.empty())
    return 1.;
  return static_cast<float>(std::accumulate(timeDeltas.begin(), timeDeltas.end(), 0.0) / timeDeltas.size());
}
